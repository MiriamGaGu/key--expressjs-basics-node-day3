require('dotenv').config();
const express = require("express");
const logger = require("morgan");

const PORT = process.env.PORT || 3000;

const app = express();

app.use(logger("dev"));

app.set("views", "./src/views");
app.set("view engine", "pug");

app.use("/static", express.static("./public"));

app.get("/", (request, response) => {
  response.render("main", {
    projectTitle: "New Title"
  });
});

app.get("/me", (request, response) => {
  response.render("me", {
    name: "Paul Irish",
    description: "Google Chrome Expert"
  });
});

app.listen(PORT, () => {
  console.log(`Running on PORT ${ PORT }`)
});
